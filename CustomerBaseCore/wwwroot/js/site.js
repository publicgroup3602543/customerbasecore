﻿
// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.




/* =================================================== */
/*                       Events                       */
/* =================================================== */

/* Событие на загрузку страницы */
$(document).ready(function () {
    ParentDocumentReady();
});

function ParentDocumentReady() {

    // the object "InputDataListHelper" depends on this event
    $('.input-dl-hlpr').each(function () {
        SetAutoWidthHendler($(this).attr('id'));
    });

    $('.selectpicker').selectpicker();


    // Call the event ".selectpicker('deselectAll')" on click button ".clr-selected"
    $('.clr-selected').on('click', function () {
        let $selPiceker = $(this).parent().prev().children('.selectpicker');

        if ($selPiceker.attr('multiple') == undefined) {
            $selPiceker.selectpicker('val', '-1');
            $selPiceker.removeClass('with-val');
        }
        else
            $selPiceker.selectpicker('deselectAll');


        $selPiceker.selectpicker('render');
        SetBorderForValueHandler();

    });


    SetSelectpickerMaxWidth();

    SetBorderForValueHandler();

}






/* Events on resize window */
$(window).resize(function () {
    // the object "InputDataListHelper" depends on this event
    $('.input-dl-hlpr').each(function () {
        SetAutoWidthHendler($(this).attr('id'));
    });

    SetSelectpickerMaxWidth();
});







/* =================================================== */
/*                   Event handlers                    */
/* =================================================== */


// Check on existing value (input, select, textarea, ...) and set class for border
/* ------------------------------------------------------------------------------------------------------------------------------ */

function SetBorderForValueHandler() {
    $('input, select, textarea, .check-value').each(function () {
        BorderForValueHandler($(this));

        $(this).unbind('change', BorderForValueHandler);
        $(this).bind('change', { $this: $(this) }, BorderForValueHandler);
    });
}



var BorderForValueHandler = function (eventObject) {

    // try to get the object, if the handler was called as an event
    let $this = eventObject.data.$this;

    // try to get the object, if the handler is used as a function
    if (!$this)
        $this = eventObject;

    let buttonSelectPicer;
    if (($this[0].tagName).toUpperCase() == "SELECT") {
        // check for bootstrap-select
        buttonSelectPicer = $this.next('button.dropdown-toggle');
    }

    if ($this.val() == "" || $this.val() == "-1" || !$this.val()) {
        $this.removeClass("with-val");
        if (buttonSelectPicer)
            buttonSelectPicer.removeClass("with-val");
    }
    else {
        $this.addClass("with-val");
        if (buttonSelectPicer)
            buttonSelectPicer.addClass("with-val");
    }

    // exclude <input typy="submit" ... >
    if ($this.attr("type") == "submit" || $this.hasClass("val-no-border")) {
        $this.removeClass("with-val");
    }

}
/* ------------------------------------------------------------------------------------------------------------------------------ */








// Event hendler onchange: <input list=...> (value = "")
// the object "InputDataListHelper" depends on this event
/* ------------------------------------------------------------------------------------------------------------------------------ */
function InputDataListOnChangeHendler(InputId) {
    $thisInput = $('#' + InputId);
    // Получить наименование datalist через атрибут list для текущего input

    let $list = $("#" + $thisInput.attr("list"));

    // Получить attrDataId из значения атрибута data-id из dataList по выбранному наименованию
    // let attrDataId = $list.find('option[value=\'' + $thisInput.val() + '\']').attr("data-id");
    let attrDataId;
    if ($thisInput.val() != "")
        $list.find('option').each(function () {
            if ($(this).attr("value") == $thisInput.val()) {
                attrDataId = $(this).attr("data-id");
                return;
            }
        });


    if (!attrDataId) {
        if ($thisInput.val() != "") {
            swal("В справочнике не найдено соответствия!", '"' + $thisInput.val() + '"', "error");
            $thisInput.css({ "backgroundColor": "coral", "color": "white" });
        }
        else if ($thisInput[0].required)
            $thisInput.css({ "backgroundColor": "coral", "color": "white" });
        else
            $thisInput.css({ "backgroundColor": "white", "color": "black" });

        $thisInput.focus();

        attrDataId = "-1";
    }
    else
        $thisInput.css({ "backgroundColor": "white", "color": "black" });

    // Присвоить attrDataId атрибуту data-id в соответствующем Input
    $thisInput.attr("data-id", attrDataId);


    // Присвоить attrDataId скрытому <input id="hiddenTaskId" ...>
    $('#hidden_' + InputId).attr("value", attrDataId);

    SetAutoWidthHendler(InputId);

}
/* ------------------------------------------------------------------------------------------------------------------------------ */














// Event hendler onload: calculate auto width for <input list=...>
// the object "InputDataListHelper" depends on this event
/* ------------------------------------------------------------------------------------------------------------------------------ */
function SetAutoWidthHendler(InputId) {
    let $thisInput = $('#' + InputId);
    let $divAutoWidth = $('#autowidth_' + InputId);
    let $divMinWidth = $('#minwidth_' + InputId);
    let $mainDiv = $("#mainDiv_" + InputId);
    let $btn = $('#btn_' + InputId);

    // get full width for button
    let btnPaddingLeft = $btn.css("padding-left").replace("px", "");
    let btnPaddingRight = $btn.css("padding-right").replace("px", "");
    let btnBorder = $btn.css("border-width").replace("px", "");
    if (!btnBorder)
        btnBorder = 1;
    let btnMarginLeft = $btn.css("margin-left").replace("px", "");
    let btnMarginRight = $btn.css("margin-right").replace("px", "");

    let btnWidth = $btn.width() + +btnPaddingLeft + +btnPaddingRight + (+btnBorder * 2) + +btnMarginLeft + +btnMarginRight;

    // set minWidthInput
    let minWidthInput = $thisInput.css("min-width").replace("px", "");
    let minWidthEtalon = 0;
    if (minWidthInput) {
        if ($divMinWidth.css("min-width") == "0px")
            $divMinWidth.css({ "min-width": minWidthInput + "px" });
        minWidthEtalon = $divMinWidth.css("min-width").replace("px", "");
        minWidthEtalon = +minWidthEtalon;
    }

    if (minWidthInput && minWidthEtalon) {
        minWidthInput = minWidthEtalon - btnWidth;
        $thisInput.css({ "min-width": minWidthInput + "px" })
    }

    // calculate auto width
    $divAutoWidth.text($thisInput.val());
    $divAutoWidth.css({
        "font-family": $thisInput.css("font-family"),
        "font-style": $thisInput.css("font-style"),
        "font-size": $thisInput.css("font-size")
    });
    let inputPaddingLeft = $thisInput.css("padding-left").replace("px", "");
    let inputPaddingRight = $thisInput.css("padding-right").replace("px", "");
    let inputMarginLeft = $thisInput.css("margin-left").replace("px", "");
    let inputMarginRight = $thisInput.css("margin-right").replace("px", "");


    let widthAutoWidth = $divAutoWidth.width() + +inputPaddingLeft + +inputPaddingRight +
        +inputMarginLeft + +inputMarginRight + 25;
    if (widthAutoWidth < minWidthInput)
        widthAutoWidth = minWidthInput;
    $thisInput.css({ "width": widthAutoWidth + "px" });
    let fullWidth = widthAutoWidth + btnWidth;

    let i = 0;
    while (i < 2) {
        let maxWidth = $(window).width() - $mainDiv.offset().left - 20;
        let mainDivWidth = fullWidth;

        if (mainDivWidth > maxWidth)
            mainDivWidth = maxWidth;

        $mainDiv.css({ "width": mainDivWidth + 1 });
        $thisInput.css({ "width": (mainDivWidth - btnWidth) + "px" });


        i = ++i;
    }
}
/* ------------------------------------------------------------------------------------------------------------------------------ */




// Event  hendler onclick: clear <input list=...> (value = "")
// the object "InputDataListHelper" depends on this event
/* ------------------------------------------------------------------------------------------------------------------------------ */
function ClearButtonOnClickHendler(InputId) {
    let $thisInputTask = $('#' + InputId);
    $thisInputTask[0].value = "";
    $thisInputTask.trigger('change');
}
/* ------------------------------------------------------------------------------------------------------------------------------ */






// Set max-width for all "selectpicker" button 
/* ------------------------------------------------------------------------------------------------------------------------------ */
function SetSelectpickerMaxWidth() {
    $('.selectpicker').each(function () {
        let $selectpicker = $(this);
        let $btnSelectpicker = $selectpicker.next();
        let $btnClrSelected;

        // get parent window width
        let parentWindow = $selectpicker.attr('data-parent-widthwindow');
        let parentWindowWidth = 0;
        let offsetParentLeft = 0;
        if (parentWindow) {
            parentWindowWidth = $(parentWindow).width();
            offsetParentLeft = $(parentWindow).offset().left;
        }
        if (!parentWindowWidth)
            parentWindowWidth = $(window).width();

        // get clear button width 
        let btnClrSelectedWidth = 0;
        $btnSelectpicker.parents('.input-group').each(function () {
            $(this).find('.input-group-append>button.clr-selected').each(function () {
                $btnClrSelected = $(this);
                return;
            });
            return;
        });

        if ($btnClrSelected)
            btnClrSelectedWidth = $btnClrSelected.width()
                + parseInt($btnClrSelected.css('padding-left')) + parseInt($btnClrSelected.css('padding-right'))
                + parseInt($btnClrSelected.css('margin-left')) + parseInt($btnClrSelected.css('margin-right'))
                + parseInt($btnClrSelected.css('border-left-width')) + parseInt($btnClrSelected.css('border-right-width'));

        // set max width for "selectpicker" button
        if ($btnSelectpicker.hasClass('dropdown-toggle')) {
            let offsetLeftBody = $btnSelectpicker.offset().left;
            let offsetLeft = offsetLeftBody - offsetParentLeft;
            let maxWidth = parentWindowWidth - offsetLeft - btnClrSelectedWidth - 5;
            $btnSelectpicker.css({ "max-width": maxWidth + 'px' });
        }

    });


}
/* ------------------------------------------------------------------------------------------------------------------------------ */

