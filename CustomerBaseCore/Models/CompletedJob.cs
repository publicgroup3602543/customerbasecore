﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerBaseCore.Models
{
    /// <summary>
    /// Модель для таблицы COMPLETED_JOB (выполненная работа в рамках посещения)
    /// </summary>
    [Table("COMPLETED_JOB")]
    public class CompletedJob
    {
        /// <summary>
        /// ID выполненной работы
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDJOB")]
        [Required]
        public int Id { get; set; }


        /// <summary>
        /// ID Факта посещения
        /// </summary>
        [Column("VISIT_ID")]
        [ForeignKey("FK_COMPLETED_JOB")]
        public int? VisitId { get; set; }
        // public Visits Visits;





        /// <summary>
        /// ID Вида выполненной работы
        /// </summary>
        [Column("TASK_ID")]
        [ForeignKey("FK_COMPLETED_JOB_1")]
        [Required]
        public int? TaskId { get; set; }
        // public Tasks Tasks;

        /// <summary>
        /// Наименование выполненной работы
        /// </summary>
        public string TaskName { get; set; } = "";



        /// <summary>
        /// Время С выполенной задачи
        /// </summary>
        [Column("FROM")]
        [Required]
        public TimeSpan? TimeFrom { get; set; }


        /// <summary>
        /// Время ДО выполенной задачи
        /// </summary>
        [Column("TILL")]
        [Required]
        public TimeSpan? TimeTill { get; set; }


        [Column("COMMENT")]
        [StringLength(4000)]
        public string Comment { get; set; }


    }
}
