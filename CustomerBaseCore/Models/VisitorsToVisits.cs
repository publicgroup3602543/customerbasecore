﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerBaseCore.Models
{
    /// <summary>
    /// Модель для таблицы VISITORS_TO_VISITS (список учкастинков факта сопровождения)
    /// </summary>
    [Table("VISITORS_TO_VISITS")]
    public class VisitorsToVisits
    {
        /// <summary>
        /// Ссылка на ID факта выполенной работы
        /// </summary>
        [Column("VISIT_ID")]
        [ForeignKey("FK_VISITORS_TO_VISITS")]
        public int VisitId { get; set; }
        // public Visits Visit { get; set; }; 

        [Column("VISITOR_ID")]
        [ForeignKey("FK_VISITORS_TO_VISITS_1")]
        public int VisitorId { get; set; }
        // public Visitors Visitor { get; set; }

    }

}
