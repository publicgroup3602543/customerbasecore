﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerBaseCore.Models
{
    /// <summary>
    /// Модель для таблицы VISIT_TYPES (справочник видов сопровождения)
    /// </summary>
    [Table("VISIT_TYPES")]
    public class VisitTypes
    {
        /// <summary>
        /// ID вида сопровождения
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDTYPE")]
        public int Id { get; set; }

        [Required]
        [StringLength(120)]
        [Column("TYPE_NAME")]
        public string TypeName { get; set;  }

    }
}
