﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerBaseCore.Models
{
    [Table("TASKS")]
    public class Tasks
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDTASK")]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        [Column("TASK_NAME")]
        public string TaskName { get; set; }

    }
}
