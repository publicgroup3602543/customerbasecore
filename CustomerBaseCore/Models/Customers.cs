﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace CustomerBaseCore.Models
{
    [Table("CUSTOMERS")]
    public class Customers
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDCUSTOMER")]
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        [Column("CUSTOMER_NAME")]
        public string CustomerName { get; set; }
        
        [StringLength(255)]
        [Column("ADDRESS")]
        public string Address { get; set; }

        [StringLength(120)]
        [Column("PHONE_NUMBER")]
        public string PhoneNumber { get; set; }

        [StringLength(512)]
        [Column("ADDITIONAL_INFO")]
        public string AdditionalInfo { get; set; }

        [Column("CURATOR_ID")]
        [ForeignKey("FK_CUSTOMERS")]
        public int? CuratorId { get; set; }
        public Visitors Visitors;


        [Column("DOG_ID")]
        public int? DogId { get; set; }


    }
}
