﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CustomerBaseCore.Models
{
    [Table("VISITORS")]
    public class Visitors
    {
        public Visitors()
        {
        }

        public Visitors(string visitorFIO)
        {
            VisitorName = visitorFIO;
            ParseFIO();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDVISITOR")]
        public int? Id { get; set; }

        [Required]
        [StringLength(255)]
        [Column("VISITOR_NAME")]
        public string VisitorName { get; set; }

        [StringLength(255)]
        [Column("VISITOR_POSITION")]
        public string VisitorPosition { get; set; }

        [StringLength(120)]
        [Column("VISITOR_CONTACT")]
        public string VisitorContact { get; set; }





        /// <summary>
        /// Фамилия посещающего
        /// </summary>
        public string Surname { get; private set; } = "";


        /// <summary>
        /// Имя посещающего
        /// </summary>
        public string Name { get; private set; } = "";



        /// <summary>
        /// Имя посещающего
        /// </summary>
        public string Secname { get; private set; } = "";



        /// <summary>
        /// Сокращенное ФИО (Иванов И.И.)
        /// </summary>
        public string VisitorFIOShort =>
           Surname +
           (Name == "" ? "" : " " + Name[0] + ".") +
           (Secname == "" ? "" : Secname[0] + ".");




        /// <summary>
        /// Метод из полного ФИО пациента инициализирует отдельно свойсва "Фамилия", "Имя", "Отчество", "ФИО Сокращенное". 
        /// </summary>
        void ParseFIO()
        {
            string visitorFIOFull = VisitorName.Trim();
            int i = 0;
            if (visitorFIOFull != "" && visitorFIOFull != null)
            {
                // Получить Фамилию
                bool isGet = false;
                while (i <= visitorFIOFull.Length - 1 && !isGet)
                {
                    if (visitorFIOFull[i] != ' ')
                        Surname += visitorFIOFull[i];
                    else
                        isGet = true;

                    i++;
                }

                // Получить Имя
                isGet = false;
                while (i <= visitorFIOFull.Length - 1 && !isGet)
                {
                    if (visitorFIOFull[i] != ' ')
                        Name += visitorFIOFull[i];
                    else
                        isGet = true;

                    i++;
                }

                // Получить Отчество
                Secname = visitorFIOFull.Substring(i);
            }

        }
    }
}
