﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CustomerBaseCore.Models;

namespace CustomerBaseCore.ModelsView
{
    /// <summary>
    /// Модель для View - Факт сопровождения 
    /// </summary>
    public class SupportFactVewModel
    {


        public Visits Visit { get; set; }




        /// <summary>
        /// Список ID посетивших объект
        /// </summary>
        public IEnumerable<int> Visitors { get; set; }





        /// <summary>
        /// Таблица "выполенная работа"
        /// </summary>
        public List<CompletedJob> CompletedJob { get; set; }








        /* ========================= */
        /* =====  Справочники  ===== */
        /* ========================= */

        /// <summary>
        /// Справочник объектов
        /// </summary>
        public List<Customers> CustomersList { get; set; }


        /// <summary>
        /// Справочник Visitors (кто сопровождал)
        /// </summary>
        public List<Visitors> VisitorList { get; set; }



        /// <summary>
        /// Справочник видов посещеий 
        /// </summary>
        public List<VisitTypes> VisitTypeList { get; set; }




        /// <summary>
        /// Справочник видов задач 
        /// </summary>
        public List<Tasks> TaskList { get; set; }




        /// <summary>
        /// Список ID visitors у которых VisitorFIOShort повторяются
        /// (Иванов Иван Иванович (Иванов И.И.), Иванов Игорь Иванович (Иванов И.И.))
        /// </summary>
        public List<int> DoubleFIOShortID
        {
            get
            {
                List<int> listFIOShortID = new List<int>();

                /* Получить коллекцию объектов, сгруппированных по повторяющимся VisitorFIOShort */
                var grByFIOShort = VisitorList?.GroupBy(v => v.VisitorFIOShort)
                    .Where(w => w.Count() > 1).ToList();

                /* Сформировать список ID visitors, у которых VisitorFIOShort повторяются */
                if (grByFIOShort != null)
                    foreach (var grBy in grByFIOShort)
                        foreach (var vistor in grBy)
                            listFIOShortID.Add((int)vistor.Id);

                return listFIOShortID;
            }
        }


    }
}
