/* begin of X *//* author */
RECONNECT;

/*UPDATE DB_VERSION SET BUILD_VERSION = X;*/
COMMIT; 
/* end of X */

/* done in script_rel-1_0_1 begin */

/* done in script_rel-1_0_1 end */

/* begin of 0 */
DELETE FROM DB_VERSION;
INSERT INTO DB_VERSION
  (MAJOR_VERSION,MINOR_VERSION,RELEASE_VERSION,BUILD_VERSION)
  VALUES(0, 0, 0, 0);

COMMIT;
/* end of 0 */

/* begin of 1 *//* narynkevich */
/* done in script_rel-1_0_1 begin */
/* #21 ����� "���� �������������". ��� ���������� ��������� � ������ ������������ ��������� ������ */
RECONNECT;
CREATE OR ALTER trigger COMPLETED_JOB_BI0 for COMPLETED_JOB
active before insert position 0
as
declare variable EXCEPTION_TEXT varchar(1024);
declare variable BEFORE_FROM time;
declare variable BEFORE_TILL time;
declare variable BEFORE_TASK_NAME varchar(256);
declare variable BEFORE_VISITOR_NAME varchar (256);
declare variable BEFORE_CUSTOMER_NAME varchar (256);
declare variable NEW_TASK_NAME varchar(256);
declare variable NEW_VISIT_DATE date;
declare variable NEW_VISITOR_ID integer;
declare variable CRLF varchar(2);
begin
  CRLF = '
';
  /* ��������: ����� ������ ���� ���������, ����� �� �� ������ ���� ������ ���� ����� ������� � */
  if ((new."FROM" is null) or (new.TILL is null)) then
    begin
      EXCEPTION_TEXT =
         '''"����� �� ���������"'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ����� ����� ����� ������ ���� ������ ������� ������ */
  if (new.TILL <= new."FROM") then
    begin
      EXCEPTION_TEXT =
         '''"����������� ������� �����: '||longsubstr(new."FROM", 1, 5)||
         ' - '||longsubstr(new.TILL, 1, 5)||'"'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ������ �� ��� ����������� ������ */
  if ((new.TASK_ID is null) or (new.TASK_ID = -1)  ) then
    begin
      EXCEPTION_TEXT =
         '''�� ������ ��� ����������� ������''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* �������� ������������ ����� ������ */
  select ts.TASK_NAME from TASKS ts
  where ts.IDTASK = new.TASK_ID
  into :NEW_TASK_NAME;

  /* �������� ���� ����� ������ */
  select cast(vs.VISIT_DATE as DATE) from VISITS vs
  where vs.IDVISIT = new.VISIT_ID
  into :NEW_VISIT_DATE;

  /* ������� �� ����� �������� "� ���� ��������� ���������" */
  if (new.TASK_ID = 20) then
    begin
      EXCEPTION_TEXT = '''������� "'||NEW_TASK_NAME||'" ��������� ��� ������.'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ������� �� ���������� ��� ������ (�������� "������...")*/
  if (((longltrim(new.COMMENT) = '') or (new.COMMENT is null)) and new.TASK_ID not in (24, 56, 57) ) then
    begin
      EXCEPTION_TEXT = '''�� �������� ����������� ��� ������: '||CRLF||
          '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||']. '||
          NEW_TASK_NAME||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ���� � ����� ������� ������ �� ������ ���� ������ ������� */
  if (cast(NEW_VISIT_DATE||' '||new."FROM" as timestamp) > cast('Now' as timestamp)) then
    begin
      EXCEPTION_TEXT = '''����� ������: "'||longsubstr(new."FROM", 1, 5)||
          '" ('||NEW_TASK_NAME||')'||CRLF||
          '������ ��������: "'||longsubstr(cast('Now' as time), 1, 5)||'"'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end


  /* ��������: ����� � ������� ������ �� ������ ������������ */
  for select
    cj."FROM",
    cj.TILL,
    ts.TASK_NAME
  from COMPLETED_JOB cj
  left join TASKS ts on cj.TASK_ID = ts.IDTASK
  where cj.VISIT_ID = new.VISIT_ID
  and cj.IDJOB <> new.IDJOB
  into
    :BEFORE_FROM,
    :BEFORE_TILL,
    :BEFORE_TASK_NAME
  do
    begin
      if (new."FROM" >= BEFORE_FROM and new."FROM" < BEFORE_TILL) then
        begin
          EXCEPTION_TEXT = '''����� ������ "'||longsubstr(new."FROM", 1, 5)||
              '" ('||NEW_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||
              '] ('||BEFORE_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (BEFORE_FROM >= new."FROM" and BEFORE_FROM < new.TILL) then
        begin
          EXCEPTION_TEXT = '''����� ������ "'||longsubstr(BEFORE_FROM, 1, 5)||
              '" ('||BEFORE_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||
              '] ('||NEW_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (new.TILL > BEFORE_FROM and new.TILL <= BEFORE_TILL) then
        begin
          EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(new.TILL, 1, 5)||
              '" ('||NEW_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||
              '] ('||BEFORE_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (BEFORE_TILL > new."FROM" and BEFORE_TILL <= new.TILL) then
        begin
          EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(BEFORE_TILL, 1, 5)||
              '" ('||BEFORE_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||
              '] ('||NEW_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end
    end

    /* ��������: ����� ������� ������ ��� ������� �����������
     * �� ������ ������������ � �������� �� ������ ��������� */
    for select  vtv.VISITOR_ID from VISITORS_TO_VISITS vtv
    where vtv.VISIT_ID = new.VISIT_ID into :NEW_VISITOR_ID
    do
      begin
        for select
          /*1*/cj2."FROM",
          /*2*/cj2.TILL,
          /*3*/ts2.TASK_NAME,
          /*4*/vtr2.VISITOR_NAME,
          /*5*/cst2.CUSTOMER_NAME
        from VISITS vs2
          left join COMPLETED_JOB cj2 on vs2.IDVISIT = cj2.VISIT_ID
          left join TASKS ts2 on cj2.TASK_ID = ts2.IDTASK
          left join VISITORS_TO_VISITS vtv2 on vs2.IDVISIT = vtv2.VISIT_ID
          left join VISITORS vtr2 on vtv2.VISITOR_ID = vtr2.IDVISITOR
          left join CUSTOMERS cst2 on vs2.CUSTOMER_ID = cst2.IDCUSTOMER
        where cast(vs2.VISIT_DATE as date) = :NEW_VISIT_DATE
          and vs2.IDVISIT <> new.VISIT_ID
          and vtv2.VISITOR_ID = :NEW_VISITOR_ID
          and ((new."FROM" >= cj2."FROM" and new."FROM" <  cj2.TILL)
                or
               (new.TILL   >  cj2."FROM" and new.TILL   <= cj2.TILL)
                or
               (cj2."FROM" >= new."FROM" and cj2."FROM" <  new.TILL)
                or
               (cj2.TILL   >  new."FROM" and cj2.TILL   <= new.TILL))
        into
          /*1*/:BEFORE_FROM,
          /*2*/:BEFORE_TILL,
          /*3*/:BEFORE_TASK_NAME,
          /*4*/:BEFORE_VISITOR_NAME,
          /*5*/:BEFORE_CUSTOMER_NAME
        do
          begin
            if (new."FROM" >= BEFORE_FROM and new."FROM" < BEFORE_TILL) then
              begin
                EXCEPTION_TEXT = '''����� ������ "'||longsubstr(new."FROM", 1, 5)||
                    ' ('||NEW_TASK_NAME||')"'||CRLF||'������������ � ���������� ���������: '||CRLF||
                    '      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      �����: ['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||'] ('||
                    BEFORE_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (new.TILL  >  BEFORE_FROM and new.TILL  <= BEFORE_TILL) then
              begin
                EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(new.TILL, 1, 5)||
                    ' ('||NEW_TASK_NAME||')" '||CRLF||'������������ � ���������� ���������: '||CRLF||
                    '      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      �����: ['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||'] ('||
                    BEFORE_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (BEFORE_FROM >= new."FROM"  and BEFORE_FROM < new.TILL) then
              begin
                 EXCEPTION_TEXT =
                  '''      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      ����� ���.: "'||longsubstr(BEFORE_FROM, 1, 5)||
                    '" ('||BEFORE_TASK_NAME||')'||CRLF||'������������ � ����������: '||CRLF||
                    '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||'] ('||
                    NEW_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (BEFORE_TILL > new."FROM"  and BEFORE_TILL <= new.TILL) then
              begin
                 EXCEPTION_TEXT =
                  '''      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      ����� �����.: "'||longsubstr(BEFORE_TILL, 1, 5)||
                    '" ('||BEFORE_TASK_NAME||')'||CRLF||'������������ � ����������: '||CRLF||
                    '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||'] ('||
                    NEW_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end
          end
      end
end; 


CREATE OR ALTER trigger COMPLETED_JOB_BU0 for COMPLETED_JOB
active before update position 0
as
declare variable EXCEPTION_TEXT varchar(1024);
declare variable BEFORE_FROM time;
declare variable BEFORE_TILL time;
declare variable BEFORE_TASK_NAME varchar(256);
declare variable BEFORE_VISITOR_NAME varchar (256);
declare variable BEFORE_CUSTOMER_NAME varchar (256);
declare variable NEW_TASK_NAME varchar(256);
declare variable NEW_VISIT_DATE date;
declare variable NEW_VISITOR_ID integer;
declare variable CRLF varchar(2);
begin
  CRLF = '
';
  /* ��������: ����� ������ ���� ���������, ����� �� �� ������ ���� ������ ���� ����� ������� � */
  if ((new."FROM" is null) or (new.TILL is null)) then
    begin
      EXCEPTION_TEXT =
         '''"����� �� ���������"'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ����� ����� ����� ������ ���� ������ ������� ������ */
  if (new.TILL <= new."FROM") then
    begin
      EXCEPTION_TEXT =
         '����������� ������� �����: '''||longsubstr(new."FROM", 1, 5)||' - '||longsubstr(new.TILL, 1, 5)||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ������ �� ��� ����������� ������ */
  if ((new.TASK_ID is null) or (new.TASK_ID = -1)  ) then
    begin
      EXCEPTION_TEXT =
         '''�� ������ ��� ����������� ������''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* �������� ������������ ����� ������ */
  select ts.TASK_NAME from TASKS ts
  where ts.IDTASK = new.TASK_ID
  into :NEW_TASK_NAME;

  /* �������� ���� ����� ������ */
  select cast(vs.VISIT_DATE as DATE) from VISITS vs
  where vs.IDVISIT = new.VISIT_ID
  into :NEW_VISIT_DATE;

  /* ������� �� ����� �������� "� ���� ��������� ���������" */
  if (new.TASK_ID = 20) then
    begin
      EXCEPTION_TEXT = '''������� "'||NEW_TASK_NAME||'" ��������� ��� ������.''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ��������: ������� �� ���������� ��� ������ (�������� "������")*/
  if (((longltrim(new.COMMENT) = '') or (new.COMMENT is null)) and new.TASK_ID not in (24, 56, 57) ) then
    begin
      EXCEPTION_TEXT = '''�� �������� ����������� ��� ������: '||CRLF||
          '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||']. '||
          NEW_TASK_NAME||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end

  /* ���� � ����� ������� ������ �� ������ ���� ������ ������� */
  if (cast(NEW_VISIT_DATE||' '||new."FROM" as timestamp) > cast('Now' as timestamp)) then
    begin
      EXCEPTION_TEXT = '''����� ������: "'||longsubstr(new."FROM", 1, 5)||
          '" ('||NEW_TASK_NAME||')'||CRLF||
          '������ ��������: "'||longsubstr(cast('Now' as time), 1, 5)||'"'||CRLF||'''';

      execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
      exception DYNAMIC_EXCEPTION;
    end


  /* ��������: ����� � ������� ������ �� ������ ������������ */
  for select
    cj."FROM",
    cj.TILL,
    ts.TASK_NAME
  from COMPLETED_JOB cj
  left join TASKS ts on cj.TASK_ID = ts.IDTASK
  where cj.VISIT_ID = new.VISIT_ID
  and cj.IDJOB <> new.IDJOB
  into
    :BEFORE_FROM,
    :BEFORE_TILL,
    :BEFORE_TASK_NAME
  do
    begin
      if (new."FROM" >= BEFORE_FROM and new."FROM" < BEFORE_TILL) then
        begin
          EXCEPTION_TEXT = '''����� ������ "'||longsubstr(new."FROM", 1, 5)||
              '" ('||NEW_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||
              '] ('||BEFORE_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (BEFORE_FROM >= new."FROM" and BEFORE_FROM < new.TILL) then
        begin
          EXCEPTION_TEXT = '''����� ������ "'||longsubstr(BEFORE_FROM, 1, 5)||
              '" ('||BEFORE_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||
              '] ('||NEW_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (new.TILL > BEFORE_FROM and new.TILL <= BEFORE_TILL) then
        begin
          EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(new.TILL, 1, 5)||
              '" ('||NEW_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||
              '] ('||BEFORE_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end

      if (BEFORE_TILL > new."FROM" and BEFORE_TILL <= new.TILL) then
        begin
          EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(BEFORE_TILL, 1, 5)||
              '" ('||BEFORE_TASK_NAME||')'||CRLF||
              '������������ � ����������:'||CRLF||
              '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||
              '] ('||NEW_TASK_NAME||')'||CRLF||'''';

          execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
          exception DYNAMIC_EXCEPTION;
        end
    end

    /* ��������: ����� ������� ������ ��� ������� �����������
     * �� ������ ������������ � �������� �� ������ ��������� */
    for select  vtv.VISITOR_ID from VISITORS_TO_VISITS vtv
    where vtv.VISIT_ID = new.VISIT_ID into :NEW_VISITOR_ID
    do
      begin
        for select
          /*1*/cj2."FROM",
          /*2*/cj2.TILL,
          /*3*/ts2.TASK_NAME,
          /*4*/vtr2.VISITOR_NAME,
          /*5*/cst2.CUSTOMER_NAME
        from VISITS vs2
          left join COMPLETED_JOB cj2 on vs2.IDVISIT = cj2.VISIT_ID
          left join TASKS ts2 on cj2.TASK_ID = ts2.IDTASK
          left join VISITORS_TO_VISITS vtv2 on vs2.IDVISIT = vtv2.VISIT_ID
          left join VISITORS vtr2 on vtv2.VISITOR_ID = vtr2.IDVISITOR
          left join CUSTOMERS cst2 on vs2.CUSTOMER_ID = cst2.IDCUSTOMER
        where cast(vs2.VISIT_DATE as date) = :NEW_VISIT_DATE
          and vs2.IDVISIT <> new.VISIT_ID
          and vtv2.VISITOR_ID = :NEW_VISITOR_ID
          and ((new."FROM" >= cj2."FROM" and new."FROM" <  cj2.TILL)
                or
               (new.TILL   >  cj2."FROM" and new.TILL   <= cj2.TILL)
                or
               (cj2."FROM" >= new."FROM" and cj2."FROM" <  new.TILL)
                or
               (cj2.TILL   >  new."FROM" and cj2.TILL   <= new.TILL))
        into
          /*1*/:BEFORE_FROM,
          /*2*/:BEFORE_TILL,
          /*3*/:BEFORE_TASK_NAME,
          /*4*/:BEFORE_VISITOR_NAME,
          /*5*/:BEFORE_CUSTOMER_NAME
        do
          begin
            if (new."FROM" >= BEFORE_FROM and new."FROM" < BEFORE_TILL) then
              begin
                EXCEPTION_TEXT = '''����� ������ "'||longsubstr(new."FROM", 1, 5)||
                    ' ('||NEW_TASK_NAME||')"'||CRLF||'������������ � ���������� ���������: '||CRLF||
                    '      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      �����: ['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||'] ('||
                    BEFORE_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (new.TILL  >  BEFORE_FROM and new.TILL  <= BEFORE_TILL) then
              begin
                EXCEPTION_TEXT = '''����� ��������� "'||longsubstr(new.TILL, 1, 5)||
                    ' ('||NEW_TASK_NAME||')" '||CRLF||'������������ � ���������� ���������: '||CRLF||
                    '      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      �����: ['||longsubstr(BEFORE_FROM, 1, 5)||'-'||longsubstr(BEFORE_TILL, 1, 5)||'] ('||
                    BEFORE_TASK_NAME||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (BEFORE_FROM >= new."FROM"  and BEFORE_FROM < new.TILL) then
              begin
                 EXCEPTION_TEXT =
                  '''      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      ����� ���.: "'||longsubstr(BEFORE_FROM, 1, 5)||
                    '" ('||BEFORE_TASK_NAME||')'||CRLF||'������������ � ����������: '||CRLF||
                    '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||'] ('||
                    longsubstr(NEW_TASK_NAME, 1, 10)||')'||CRLF||'''';

                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end

            if (BEFORE_TILL > new."FROM"  and BEFORE_TILL <= new.TILL) then
              begin
                 EXCEPTION_TEXT =
                  '''      ������: "'||BEFORE_CUSTOMER_NAME||'"'||CRLF||
                    '      ���: "'||BEFORE_VISITOR_NAME||'"'||CRLF||
                    '      ����� �����.: "'||longsubstr(BEFORE_TILL, 1, 5)||
                    '" ('||BEFORE_TASK_NAME||')'||CRLF||'������������ � ����������: '||CRLF||
                    '['||longsubstr(new."FROM", 1, 5)||'-'||longsubstr(new.TILL, 1, 5)||'] ('||
                    longsubstr(NEW_TASK_NAME, 1, 10)||')'||CRLF||'''';


                execute statement 'alter exception DYNAMIC_EXCEPTION '||:EXCEPTION_TEXT;
                exception DYNAMIC_EXCEPTION;
              end
          end
      end
end;
/* done in script_rel-1_0_1 end */
UPDATE DB_VERSION SET BUILD_VERSION = 1;
COMMIT; 
/* end of 1 */


/* begin of 2 *//* narynkevich */
RECONNECT;
/* done in script_rel-1_0_1 begin */
/* #22  "����� � ����������� ������". � ����� "����������� ������" ��� ������ ������ ������ "---" ������ ����� [01:00 - 05:00] */
RECONNECT;
CREATE OR ALTER procedure GET_COMMENTS_BY_VISIT_ID (
    VISITID integer)
returns (
    VISITNO integer,
    COMMENTS varchar(32765))
as
declare variable TASK varchar(32765);
declare variable TASK_COMMENT varchar(4000);
declare variable J_FROM time;
declare variable J_TILL time;
BEGIN
  COMMENTS = '';
  FOR SELECT T.TASK_NAME, J.COMMENT, j."FROM", j.TILL FROM COMPLETED_JOB J
    LEFT JOIN TASKS T ON (J.TASK_ID = T.IDTASK)
    WHERE J.VISIT_ID = :VISITID
    ORDER BY J."FROM"
    INTO :TASK, :TASK_COMMENT, :J_FROM, :J_TILL
  DO
  BEGIN

    IF (TASK_COMMENT IS NULL) THEN TASK_COMMENT = '';

    IF (TASK IS NULL) THEN
      TASK = TASK_COMMENT;
    ELSE
      TASK = '['||LONGSUBSTR(J_FROM, 1, 5)||'-'||LONGSUBSTR(J_TILL, 1, 5)||'] '||TASK;

    IF (TASK IS NOT NULL) THEN TASK = TASK||' '||TASK_COMMENT;

    IF (COMMENTS <> '') THEN
        COMMENTS = COMMENTS||'. '||'
'||TASK;
    IF (COMMENTS = '') THEN
      COMMENTS = TASK;
    COMMENTS = LONGLTRIM(COMMENTS);
  END
  VISITNO = :VISITID;
  SUSPEND;
END;

/* done in script_rel-1_0_1 end */
UPDATE DB_VERSION SET BUILD_VERSION = 2;
COMMIT; 
/* end of 2 */


/* begin of 3 *//* narynkevich */
RECONNECT;
/* done in script_rel-1_0_1 begin */

DECLARE EXTERNAL FUNCTION LONGC
    CSTRING(16383)
RETURNS CSTRING(16383)
ENTRY_POINT 'fn_c' MODULE_NAME 'rfunc';

/* done in script_rel-1_0_1 end */
UPDATE DB_VERSION SET BUILD_VERSION = 3;
COMMIT; 
/* end of 3 */


/* begin of 4 *//* narynkevich */
RECONNECT;
/* done in script_rel-1_0_1 begin */

CREATE DOMAIN TTIME AS TIME;

/******************************************************************************/
/***               Generated by IBExpert 27.09.2019 10:25:01                ***/
/******************************************************************************/

/******************************************************************************/
/***      Following SET SQL DIALECT is just for the Database Comparer       ***/
/******************************************************************************/
SET SQL DIALECT 3;



/******************************************************************************/
/***                                 Tables                                 ***/
/******************************************************************************/



CREATE TABLE S$_GROW (
    GROW_DATE   TDATE NOT NULL /* TDATE = DATE */,
    D_FROM      TTIME /* TTIME = TIME */,
    D_TILL      TTIME /* TTIME = TIME */,
    VISITOR_ID  TINT_NOT_NULL /* TINT_NOT_NULL = INTEGER NOT NULL */
);




/******************************************************************************/
/***                              Primary keys                              ***/
/******************************************************************************/

ALTER TABLE S$_GROW ADD CONSTRAINT PK_S$_GROW PRIMARY KEY (GROW_DATE);


/******************************************************************************/
/***                              Foreign keys                              ***/
/******************************************************************************/

ALTER TABLE S$_GROW ADD CONSTRAINT FK_S$_GROW_1 FOREIGN KEY (VISITOR_ID) REFERENCES VISITORS (IDVISITOR);


/******************************************************************************/
/***                               Privileges                               ***/
/******************************************************************************/


/* done in script_rel-1_0_1 end */

UPDATE DB_VERSION SET BUILD_VERSION = 4;
COMMIT; 
/* end of 4 */
