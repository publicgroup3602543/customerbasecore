﻿using CustomerBaseCore.ModelsView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerBaseCore.ContextDB
{
    abstract public class ContextDB
    {
        protected readonly IDbConnection _connection;

        protected ContextDB(IDbConnection connection)
        {
            _connection = connection;
        }

    }
}
