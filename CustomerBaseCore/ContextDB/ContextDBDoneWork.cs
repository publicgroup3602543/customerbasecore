﻿using CustomerBaseCore.Models;
using CustomerBaseCore.ModelsView;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerBaseCore.ContextDB
{
    public class ContextDBDoneWork : ContextDB
    {
        public ContextDBDoneWork(IDbConnection connection) : base(connection)
        {
        }


        /// <summary>
        /// Метод: Получить данные для "Отчет о проделанной работе", передав тольк IdVisit
        /// </summary>
        /// <returns></returns>
        public List<DoneWorkRow> SelectForDoneWork(int IdVisit = -1)
        {
            return SelectForDoneWork(
                dateFrom: DateTime.MinValue, dateTill: DateTime.MinValue,
                idVisit: IdVisit, withoutTime: false);
        }



        /// <summary>
        /// Метод: Получить данные для "Отчет о проделанной работе"
        /// </summary>
        /// <returns></returns>
        public List<DoneWorkRow> SelectForDoneWork(DateTime dateFrom, DateTime dateTill,
                                                   int? visitorId = -1, int? customerId = -1, int idVisit = -1, bool withoutTime = false)
        {

            var doneWorkRow = new List<DoneWorkRow>();

            // Текст запроса к БД - отчет о проделанной работе
            var queryOptions = @"";
            if (idVisit != -1)
                queryOptions = "\n  where v.IDVISIT = " + idVisit.ToString();
            else
                queryOptions = "\n" + @"  where v.VISIT_DATE >= @dateFrom and v.VISIT_DATE < @dateTill" +
                        (visitorId != -1 ? "\n" + @"  and gv.VISITORSLIST_ID like '%,'||@visitorId||',%'" : "") +
                        (customerId != -1 ? "\n" + @"  and v.CUSTOMER_ID = @CustomerId" : "") +
                        "\n  order by v.VISIT_DATE desc";

            var sqlQueryReport = @"
                        select
                          v.IDVISIT,
                          cast(v.VISIT_DATE as date) as VISIT_DATE,
                          vt.TYPE_NAME,
                          ct.CUSTOMER_NAME as CUSTOMER_NAME,
                          gv.ALLVISITORS
                        from VISITS v
                          left join CUSTOMERS ct on v.CUSTOMER_ID = ct.IDCUSTOMER
                          left join GET_VISITORS_BY_VISIT_ID(v.IDVISIT) gv on v.IDVISIT = gv.VISITNO
                          left join VISIT_TYPES vt on v.VISIT_TYPE = vt.IDTYPE
                        " +
                        queryOptions;

            using (var command = _connection.CreateCommand())
            {

                command.CommandText = sqlQueryReport;

                // Передача параметров запросу
                command.AddParameter("@dateFrom", dateFrom, DbType.Date);
                command.AddParameter("@dateTill", dateTill, DbType.Date);
                command.AddParameter("@visitorId", visitorId, DbType.Int32); 
                command.AddParameter("@CustomerId", customerId, DbType.Int32);

                var commandTextFull = command.CommandText;

                _connection.Open();

                var reader = command.ExecuteReader();

                // reader: Построчно считать данные и передать в модель
                while (reader.Read()) // построчное считывание данных
                {
                    doneWorkRow.Add(
                        new DoneWorkRow
                        {
                            VisitId = (int)reader["IDVISIT"],
                            VisitDate = (DateTime)reader["VISIT_DATE"],
                            TypeName = (string)reader["TYPE_NAME"],
                            CustomerName = (string)reader["CUSTOMER_NAME"],
                            AllVisitors = (string)reader["ALLVISITORS"],
                            IsNewRow = idVisit == -1 ? false : true,
                            WithoutTime = withoutTime
                        }); 
                }

                _connection.Close();
            }

            // Инициализировать объект CompletedJobs для каждой записи отчета
            for (int row = 0; row < doneWorkRow.Count; row++ )
            {
                doneWorkRow[row].CompletedJobs = SelectFromCompletedJobByIdVisit(doneWorkRow[row].VisitId);
            }

            return doneWorkRow;
        }




        public int InsertsSupportFact(SupportFactVewModel supportFact)
        {
            var isNewSupportFact = false;

            int idVisit = supportFact.Visit.Id;
            if (idVisit == -1)
                isNewSupportFact = true;

            using (var command = _connection.CreateCommand())
            {
                _connection.Open();
                var sqlQuery = "";
                var sqlTmp = "";

                /* Выполнение всех insert в рамках одной транзакции */
                using (var transaction = _connection.BeginTransaction())
                {
                    command.Transaction = transaction;

                    try
                    {
                        /* ========================= */
                        /*    Commands for Visits    */
                        /* ========================= */

                        // Check parametrs on "null" or "-1"
                        var idVisitStr = idVisit == -1
                            ? "null"
                            : idVisit.ToString();
                        var vistDate = supportFact.Visit.VistDate == null || supportFact.Visit.VistDate == DateTime.MinValue
                            ? "null"
                            : "'" + supportFact.Visit.VistDate.ToString() + "'";

                        var customerID = supportFact.Visit.CustomerID == -1 || supportFact.Visit.CustomerID == null
                            ? "null"
                            : supportFact.Visit.CustomerID.ToString();

                        var vistiTypeID = supportFact.Visit.VistiTypeID == -1 || supportFact.Visit.VistiTypeID == null
                            ? "null"
                            : supportFact.Visit.VistiTypeID.ToString();

                        // Set text for insert query
                        sqlTmp =
                            $"UPDATE OR INSERT INTO VISITS(IDVISIT, CUSTOMER_ID, VISIT_DATE, VISIT_TYPE) \n" +
                            $"       VALUES(COALESCE({idVisitStr}, gen_id(GEN_VISIT_ID, 1)), {customerID}, " +
                                        $"{vistDate}, {vistiTypeID})\n" +
                            $"returning IDVISIT;\n\n";

                        // Compose text for common query
                        sqlQuery += sqlTmp;

                        command.CommandText = sqlTmp;

                        idVisit = (Int32)command.ExecuteScalar();



                        /* ========================= */
                        /* Commands for CompletedJob */
                        /* ========================= */
                        string listId = "";

                        if (supportFact.CompletedJob == null & isNewSupportFact)
                            throw new Exception("\nНе внесено ни одного вида выполненных работ!");

                        if (supportFact.CompletedJob == null)
                        {
                            sqlTmp =
                                $"delete from COMPLETED_JOB cj where cj.VISIT_ID = {idVisit};\n\n";

                            sqlQuery += sqlTmp;

                            command.CommandText = sqlTmp;
                            command.ExecuteNonQuery();
                        }


                        if (supportFact.CompletedJob != null)
                        {
                            if (!isNewSupportFact)
                                foreach (var сompletedJob in supportFact.CompletedJob)
                                {
                                    if (сompletedJob.Id != -1)
                                    {
                                        listId += сompletedJob.Id.ToString() + ",";
                                    }

                                }

                            if (listId != "")
                            {
                                listId = listId.Substring(0, listId.Length - 1);
                                sqlTmp =
                                    $"delete from COMPLETED_JOB cj where cj.VISIT_ID = {idVisit} and cj.IDJOB not in ({listId});\n\n";

                                sqlQuery += sqlTmp;

                                command.CommandText = sqlTmp;
                                command.ExecuteNonQuery();
                            }

                            foreach (var сompletedJob in supportFact.CompletedJob)
                            {
                                // Check parametrs on "null" or "-1"
                                var jobId = сompletedJob.Id == -1
                                    ? "null"
                                    : сompletedJob.Id.ToString();

                                if (jobId != "null")
                                    listId += jobId + ",";


                                var taskId = сompletedJob.TaskId == -1 || сompletedJob.TaskId == null
                                    ? "null"
                                    : сompletedJob.TaskId.ToString();

                                var timeFrom = сompletedJob.TimeFrom == null || сompletedJob.TimeFrom == TimeSpan.MinValue
                                    ? "null"
                                    : "'" + сompletedJob.TimeFrom.ToString() + "'";

                                var timeTill = сompletedJob.TimeTill == null || сompletedJob.TimeTill == TimeSpan.MinValue
                                    ? "null"
                                    : "'" + сompletedJob.TimeTill.ToString() + "'";

                                // Set text for insert query
                                sqlTmp =
                                    $"UPDATE OR INSERT INTO COMPLETED_JOB(IDJOB, VISIT_ID, TASK_ID, \"FROM\", TILL, COMMENT)\n" +
                                    $"       VALUES(COALESCE({jobId}, gen_id(GEN_JOB_ID, 1)), {idVisit} , {taskId}, " +
                                             $"{timeFrom}, {timeTill}, @comment);\n\n";

                                sqlQuery += sqlTmp; // Compose text for common query

                                command.CommandText = sqlTmp;
                                command.Parameters.Clear();
                                command.AddParameter("@comment", сompletedJob.Comment, DbType.String); 
                                command.ExecuteNonQuery();
                            }

                        }





                        /* ========================= */
                        /*   Commands for Visitors   */
                        /* ========================= */
                        listId = "";
                        if (supportFact.Visitors != null)
                        {
                            //  Delete deleted items of VISITORS_TO_VISITS table
                            if (!isNewSupportFact)
                            {
                                foreach (var visitorId in supportFact.Visitors)
                                    listId += visitorId + ",";

                                if (listId != "")
                                {
                                    listId = listId.Substring(0, listId.Length - 1);
                                    sqlTmp =
                                        $"delete from VISITORS_TO_VISITS vtv " +
                                        $"where vtv.VISIT_ID = {idVisit} " +
                                        $"  and vtv.VISITOR_ID not in ({listId});\n\n";
                                    sqlQuery += sqlTmp;

                                    command.CommandText = sqlTmp;
                                    command.ExecuteNonQuery();
                                }
                            }

                            foreach (var visitorId in supportFact.Visitors)
                            {
                                sqlTmp = $"UPDATE OR INSERT INTO VISITORS_TO_VISITS(VISIT_ID, VISITOR_ID) VALUES({idVisit}, {visitorId});\n";
                                sqlQuery += sqlTmp;

                                command.CommandText = sqlTmp;
                                command.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            sqlTmp = $"INSERT INTO VISITORS_TO_VISITS(VISIT_ID, VISITOR_ID) VALUES({idVisit}, null);\n";
                            sqlQuery += sqlTmp;

                            command.CommandText = sqlTmp;
                            command.ExecuteNonQuery();

                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        var message = ex.Message + "\n\n" + sqlQuery;
                        throw;
                    }
                }

                _connection.Close();
            }

            return idVisit;

        }








        /// <summary>
        /// select from VISITS
        /// </summary>
        /// <returns></returns>
        public Visits SelectFromVisitsByIdVisit(int? idVisit)
        {
            if (idVisit == -1 || idVisit == null)
                return
                    new Visits
                    {
                        Id = -1,
                        CustomerID = -1,
                        VistDate = DateTime.Now,
                        VistiTypeID = -1
                    };

            var visit = new Visits();

            using (var command = _connection.CreateCommand())
            {
                _connection.Open();
                var sqlQuery = "";

                try
                {
                    // Text of deleting query
                    sqlQuery =
                        $"select IDVISIT, CUSTOMER_ID, VISIT_DATE, VISIT_TYPE \n" +
                        $"from VISITS v where v.IDVISIT = {idVisit.ToString()};";

                    command.CommandText = sqlQuery;
                    var reader = command.ExecuteReader();

                    // reader: Cчитать данные и передать в модель
                    while (reader.Read())
                    {
                        visit.Id = (int)reader["IDVISIT"];
                        visit.CustomerID = (int)reader["CUSTOMER_ID"];
                        visit.VistDate = (DateTime)reader["VISIT_DATE"];
                        visit.VistiTypeID = (int)reader["VISIT_TYPE"];
                    }
                }
                catch (Exception ex)
                {
                    var message = ex.Message + "\n\n" + sqlQuery;
                    throw;
                }

                _connection.Close();
            }

            return visit;
        }








        /// <summary>
        /// select from VISITORS_TO_VISITS
        /// </summary>
        /// <returns></returns>
        public IEnumerable<int> SelectFromVisitorsToVisitsByIdVisit(int? idVisit)
        {
            if (idVisit == -1 || idVisit == null)
                return
                    new List<int>() { -1 };


            var visitorsToVisits = new List<int>();

            using (var command = _connection.CreateCommand())
            {
                _connection.Open();
                var sqlQuery = "";

                try
                {
                    sqlQuery =
                        $"select vtv.VISITOR_ID from VISITORS_TO_VISITS vtv \n " +
                        $"where vtv.VISIT_ID = {idVisit.ToString()};";

                    command.CommandText = sqlQuery;
                    var reader = command.ExecuteReader();

                    // reader: Cчитать данные и передать в модель
                    while (reader.Read())
                    {
                        visitorsToVisits.Add((int)reader["VISITOR_ID"]);
                    }
                }
                catch (Exception ex)
                {
                    var message = ex.Message + "\n\n" + sqlQuery;
                    throw;
                }

                _connection.Close();
            }

            return visitorsToVisits;
        }







        /// <summary>
        /// select from COMPLETED_JOB
        /// </summary>
        /// <returns></returns>
        public List<CompletedJob> SelectFromCompletedJobByIdVisit(int idVisit = -1)
        {
            var competedJobList = new List<CompletedJob>();
            if (idVisit == -1)
            {
                competedJobList.Add(
                    new CompletedJob
                    {
                        Id = -1,
                        VisitId = idVisit,
                        TaskId = -1,
                        TaskName = "",
                        TimeFrom = TimeSpan.Zero,
                        TimeTill = TimeSpan.Zero,
                        Comment = ""

                    });
                return competedJobList;
            }


            using (var command = _connection.CreateCommand())
            {
                _connection.Open();
                var sqlQuery = "";

                try
                {
                    // Text of deleting query
                    sqlQuery =
                        $"select IDJOB, VISIT_ID, TASK_ID, t.TASK_NAME, \"FROM\", \"TILL\", LONGC(\"COMMENT\") as \"COMMENT\" \n" +
                        $"from COMPLETED_JOB cj \n" +
                        $"  left join TASKS t on cj.TASK_ID = t.IDTASK \n " +
                        $"where cj.VISIT_ID = {idVisit.ToString()} \n" +
                        $"order by \"FROM\";";

                    command.CommandText = sqlQuery;
                    var reader = command.ExecuteReader();

                    // reader: Cчитать данные и передать в модель
                    while (reader.Read())
                    {
                        competedJobList.Add(
                            new CompletedJob
                            {
                                Id = (int)reader["IDJOB"],
                                VisitId = (int)reader["VISIT_ID"],
                                TaskId = (int)reader["TASK_ID"],
                                TaskName = (string)reader["TASK_NAME"],
                                TimeFrom = (TimeSpan)reader["FROM"],
                                TimeTill = (TimeSpan)reader["TILL"],
                                Comment = (string)reader["COMMENT"]
                            });
                    }
                }
                catch (Exception ex)
                {
                    var message = ex.Message + "\n\n" + sqlQuery;
                    throw;
                }

                _connection.Close();
            }

            return competedJobList;
        }






        /// <summary>
        /// Deletеs the support fact row from DB
        /// </summary>
        /// <returns></returns>
        public void DeleteSupportFact(int idVisit)
        {
            using (var command = _connection.CreateCommand())
            {
                _connection.Open();
                var sqlQuery = "";

                /* Deletion within one transaction */
                using (var transaction = _connection.BeginTransaction())
                {
                    command.Transaction = transaction;
                    try
                    {
                        // Text of deleting query
                        sqlQuery =
                            $"delete from VISITS v where v.IDVISIT = {idVisit};";

                        command.CommandText = sqlQuery;
                        command.ExecuteNonQuery();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        var message = ex.Message + "\n\n" + sqlQuery;
                        throw;
                    }
                }

                _connection.Close();
            }

        }

    }
}
