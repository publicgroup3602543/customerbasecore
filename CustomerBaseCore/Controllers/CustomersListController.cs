﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions;
using CustomerBaseCore.Models;
using FirebirdSql.Data.FirebirdClient;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CustomerBaseCore.ContextDB;

namespace CustomerBaseCore.Controllers
{
    [Route("Settings")]
    public class CustomersListController : Controller
    {

        private ContextDBLexicon _contextDBLexicon;


        public CustomersListController(ContextDBLexicon contextDBLexicon)
        {
            _contextDBLexicon = contextDBLexicon;
        }

        [HttpGet]
        [Route("CustomersList")]
        public IActionResult IndexCustomersList()
        {            
            return View(_contextDBLexicon.GetCustomers());
        }
    }
}