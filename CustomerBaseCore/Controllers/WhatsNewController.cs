﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerBaseCore.Controllers
{
    [Route("WhatsNew")]
    public class WhatsNewController : Controller
    {
        // GET: WhatsNew
        [Route("")]
        public ActionResult IndexWhatsNew()
        {
            return View();
        }

    }
}