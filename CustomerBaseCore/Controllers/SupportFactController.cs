﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CustomerBaseCore.ContextDB;
using CustomerBaseCore.Models;
using CustomerBaseCore.ModelsView;
using Microsoft.AspNetCore.Mvc;

namespace CustomerBaseCore.Controllers
{
    [Route("SupportFact")]
    public class SupportFactController : Controller
    {

        private readonly ContextDBDoneWork _сontextDBDoneWork;
        private readonly ContextDBLexicon _contextDBLexicon;

        public SupportFactController(
            ContextDBDoneWork сontextDBDoneWork,
            ContextDBLexicon contextDBLexicon)
        {
            _сontextDBDoneWork = сontextDBDoneWork;
            _contextDBLexicon = contextDBLexicon;
        }



        [Route("NewOrOld")]
        [HttpGet]
        public IActionResult IndexSupportFact(int idVisit = -1)
        {

            // Определение Model для View
            var modelVew = new SupportFactVewModel()
            {
                Visit = _сontextDBDoneWork.SelectFromVisitsByIdVisit(idVisit),
                CompletedJob = _сontextDBDoneWork.SelectFromCompletedJobByIdVisit(idVisit),
                Visitors = _сontextDBDoneWork.SelectFromVisitorsToVisitsByIdVisit(idVisit),

                CustomersList = _contextDBLexicon.GetCustomers(),
                VisitorList = _contextDBLexicon.GetVistors(),
                VisitTypeList = _contextDBLexicon.GetVisitTypes(),
                TaskList = _contextDBLexicon.GetTasks()
            };

            return View(modelVew);
        }









        [HttpPost]
        [Route("Create")]
        public IActionResult IndexSupportFact(SupportFactVewModel supportFact)
        {
            try
            {
                var IdVisit = _сontextDBDoneWork.InsertsSupportFact(supportFact);
                return Json(IdVisit);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        [HttpDelete]
        [Route("Delete")]
        public IActionResult DeleteSupportFact(int idVisit)
        {
            // Deletе the support fact row from DB
            try
            {
                _сontextDBDoneWork.DeleteSupportFact(idVisit);
                return StatusCode(204);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


    }
}
