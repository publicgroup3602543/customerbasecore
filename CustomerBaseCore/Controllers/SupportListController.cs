﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerBaseCore.Controllers
{
    [Route("Protocol")]
    public class SupportListController : Controller
    {
        private readonly IDbConnection _connection;

        public SupportListController(IDbConnection connection)
        {
            _connection = connection;
        }


        // GET: SupportList
        [HttpGet]
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        // GET: SupportList/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SupportList/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SupportList/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SupportList/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: SupportList/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: SupportList/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SupportList/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}