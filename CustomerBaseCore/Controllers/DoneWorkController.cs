﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CustomerBaseCore.ModelsView;
using System.Data;
using CustomerBaseCore.Models;
using CustomerBaseCore.ContextDB;

namespace CustomerBaseCore.Controllers
{
    [Route("Reports")]
    [Route("~/")]
    public class DoneWorkController : Controller
    {
        private readonly ContextDBDoneWork _сontextDBDoneWork;
        private readonly ContextDBLexicon _contextDBLexicon;

        public DoneWorkController(
            ContextDBDoneWork сontextDBDoneWork,
            ContextDBLexicon contextDBLexicon)
        {
            _сontextDBDoneWork = сontextDBDoneWork;
            _contextDBLexicon = contextDBLexicon;
        }

        [HttpGet]
        [Route("")]
        public IActionResult IndexDoneWork()
        {
            return
               IndexDoneWork(new DoneWorkViewModel { DateFrom = DateTime.MinValue, DateTill = DateTime.MinValue });
        }



        [HttpGet]
        [Route("DoneWork")]
        public IActionResult IndexDoneWork(DoneWorkViewModel doneWork)
        {

            if (!ModelState.IsValid)
            {
                return Content("Модель не валидна!!!");
            }


            // Инициализация свойств для модели из БД

            // Перечень кураторов для выпадающего списка
            doneWork.VisitorList = _contextDBLexicon.GetVistors();
            // Список объектов
            doneWork.CustomersList = _contextDBLexicon.GetCustomers();


            // Контроль входных параметров
            if (doneWork.DateFrom == DateTime.MinValue)
                doneWork.DateFrom = DateTime.Today.AddDays(-7);

            if (doneWork.DateTill == DateTime.MinValue)
                doneWork.DateTill = DateTime.Today.AddDays(1);

            if (doneWork.DateFrom >= doneWork.DateTill)
            {
                var message = "Дата \"c\" (" + doneWork.DateFrom.ToShortDateString() +
                             ") должна быть меньше даты " +
                             "\"до\" (" + doneWork.DateTill.ToShortDateString() + ")";
                ModelState.AddModelError("DateTill", message);
                return View(doneWork);
            }

            if (doneWork.ParamVisitorId == null)
                doneWork.ParamVisitorId = -1;

            if (doneWork.ParamCustomerId == null)
                doneWork.ParamCustomerId = -1;

            // Данные для отчета
            doneWork.ProgressReportList =
                _сontextDBDoneWork.SelectForDoneWork(doneWork.DateFrom, doneWork.DateTill,
                                                     doneWork.ParamVisitorId, doneWork.ParamCustomerId,
                                                     withoutTime: doneWork.WithoutTime);


            // Передача данных модели
            return View(doneWork);
        }


        [HttpGet]
        [Route("DoneWorkById")]
        public IActionResult IndexDoneWork(int idVisit)
        {
            // Данные для отчета по Id факта выполенной работы
            List<DoneWorkRow> progressReportById =
                _сontextDBDoneWork.SelectForDoneWork(idVisit);

            // Передача данных модели
            return PartialView("_ProgressReportListPartial", progressReportById);
        }



    }
}




